# Change Log

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

<a name="0.2.0"></a>
# 0.2.0 (2017-09-16)


### Bug Fixes

* Fixed an dependency error and pluralize feed if there are more than one. ([81d3472](https://gitlab.com/littlefork/littlefork-plugin-instagram/commit/81d3472))


### Features

* Control the amount of posts to fetch. ([cd699b4](https://gitlab.com/littlefork/littlefork-plugin-instagram/commit/cd699b4))
* Fetch posts for multiple sources. ([ff7d4fc](https://gitlab.com/littlefork/littlefork-plugin-instagram/commit/ff7d4fc))
* Parse hashtags and mentions from post. ([3ab1249](https://gitlab.com/littlefork/littlefork-plugin-instagram/commit/3ab1249))
* Parse posts as LF entities. ([de59d89](https://gitlab.com/littlefork/littlefork-plugin-instagram/commit/de59d89))
* Prevent to make too many requests to Instagram. ([b04873e](https://gitlab.com/littlefork/littlefork-plugin-instagram/commit/b04873e))
* Read out queries from the envelope. ([5e3db74](https://gitlab.com/littlefork/littlefork-plugin-instagram/commit/5e3db74))
* Set entity relations. ([3c8562e](https://gitlab.com/littlefork/littlefork-plugin-instagram/commit/3c8562e))

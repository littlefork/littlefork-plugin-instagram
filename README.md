# `littlefork-plugin-instagram`

- [Plugins](#plugins)
- [Development](#development)

## Plugins

### `instagram_feed`

Fetch posts for an Instagram user feed. The `query_type` is
`instagram_user`. It has the `instagram.post_count`, which determines how many
posts are fetched. It defaults to 100.

    littlefork -Q instagram_user:<username> \
               -p instagram_feed \
               --instagram.post_count 50

## Development

This scaffolding builds a CommonJS module that runs on NodeJS.

There are the following `npm` scripts:

-   `watch` - Run a watcher for the tests.
-   `test` - Run all specs in `test/`.
-   `lint-docs` - Lint the [JSDoc](http://usejsdoc.org) docstrings using
    [Documentation](https://github.com/documentationjs/documentation).
-   `lint-src` - Use [ESLint](https://eslint.org/) and
    [Prettier](https://github.com/prettier/prettier) to enforce the coding
    style.
-   `lint` - Run `lint-docs` and `lint-src`.
-   `fix` - Automatically fix linting errors in the JavaScript code.
-   `clean` - Remove all compiled bundles.
-   `docs` - Build the API docs using
    [Documentation](https://github.com/documentationjs/documentation).
-   `compile` - Compile the ES6 sources using [Babel](https://babeljs.io/) using
    [rollup](https://rollupjs.org/). Runs the `clean` target before compilation.
-   `build` - Build the whole bundle. This lints, tests, documents and compiles
    the while package.
-   `check` - Test that ESLint and Prettier are in alignment.
-   `publish` - Publish to the [NPM repository](https://www.npmjs.com/).
-   `release` - Make a new release using [Conventional
    Commits](https://conventionalcommits.org/).
